import React from "react";

import {ThemeProvider} from "styled-components";
import {DARK_MODE, useDarkMode} from "./components/useDarkMode"
import {GlobalStyles} from "./components/Globalstyle";
import {lightTheme, darkTheme} from "./components/Themes"
import "./App.css";
import Home from "./components/Home";

const App = () => {
    const [theme, toggleTheme] = useDarkMode();
    const themeMode = theme === DARK_MODE.light ? lightTheme : darkTheme;

    return (
        <ThemeProvider theme={themeMode}>
            <>
                <GlobalStyles/>
                <div data-testid="App" className="App">
                    <Home
                        theme={theme}
                        toggleTheme={toggleTheme}
                    />
                </div>
            </>
        </ThemeProvider>

    );
};

export default App;
