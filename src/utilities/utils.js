export function getUserProfileDetails(user) {
    if (user) {
        return {
            avatar: user.profile_image,
            name: user.display_name,
            reputation: user.reputation,
            acceptRate: user.accept_rate,
        }
    }
    return null;
}