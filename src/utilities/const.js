export const LOCAL_STORAGE = {
    theme: 'theme'
}

export const SORT_MODE = {
    date: 'Date',
    answers: 'Answers',
    views: 'Views',
}