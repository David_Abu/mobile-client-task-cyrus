/* eslint-disable import/prefer-default-export */
import {BaseService} from './base';

/**
 * Service used for any user related calls.
 */
export default class StackOverflowService extends BaseService {
    constructor() {
        super('https://api.stackexchange.com');
    }

    /**
     * Get channel user questions
     * @returns A promise containing user questions.
     *
     */
    getUserQuestions(id, data) {
        return this.get(`2.3/users/${id}/questions?order=desc&sort=activity&site=stackoverflow`, data);
    }

}