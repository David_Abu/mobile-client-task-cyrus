import axios from 'axios';


export class BaseService {
    constructor(serviceUrl) {

        this.serviceUrl = serviceUrl;
        this.headers = {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
            'pragma': 'no-cache',
            // Access-Control-Allow-Origin "*"
        };
        this._requestConfig = {
            headers: this.headers
        };
    }

    /**
     * Performs HTTP GET to the specified endpoint.
     * @param {string} endpoint Endpoint where the resource should be fetched.
     * @param {object} queryParams Any query parameter to send with the request.
     * @returns The response from the server.
     */
    get(endpoint = '', queryParams = null, requestConfig = {}) {

        let reqConfig = Object.assign({}, this._requestConfig, requestConfig);
        reqConfig.params = queryParams;
        return axios.get(`${this.serviceUrl}/${endpoint}`, reqConfig)
            .then(res => res.data)
            .catch(err => {
                if (err && err.response) {
                    throw err.response.data;
                }
                throw err;
            });
    }

}