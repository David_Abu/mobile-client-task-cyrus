import {createGlobalStyle} from "styled-components"

export const GlobalStyles = createGlobalStyle`
  body {

    font-family: Tahoma, Helvetica, Arial, Roboto, sans-serif;
    transition: all 0.50s linear;
  }

  .App {
    background: ${({theme}) => theme.body};
    color: ${({theme}) => theme.text};
  }
`