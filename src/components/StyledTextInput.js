import React, {useState} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import TextInput from "./TextInput";
import Button from 'react-bootstrap/Button';

const StyledTextInputWrap = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 0.5em;
`;

const StyledTextInputFieldWrap = styled.div`
  position: relative;
`;


const ClearButton = styled(Button)`
  font-size: 10px;
  border-radius: 50%;
  padding: 4px;
  position: absolute;
  top: 10px;
  right: 0;
  background-color: white
`;


function StyledTextInput({
                             className = null,
                             placeholder,
                             onClear,
                             input,
                             onEnter,
                             ...props
                         }) {
    const [refs, setRefs] = useState(null);

    return (
        <StyledTextInputWrap
            className={className}
            onClick={() => refs.click()}
        >
            <StyledTextInputFieldWrap
            >
                <TextInput
                    setRef={(ref) => {
                        setRefs(ref);
                    }}
                    value={input}
                    {...props}
                    onEnter={onEnter}
                    placeholder={placeholder}
                />
                <ClearButton
                    variant='white'
                    className="btn-close btn-secondary" aria-label="Close"
                    onClick={(e) => {
                        e.target.blur();
                        onClear();
                    }}
                />

            </StyledTextInputFieldWrap>
        </StyledTextInputWrap>
    );
}


StyledTextInput.propTypes = {
    className: PropTypes.string,
    input: PropTypes.string,
    placeholder: PropTypes.string,
    onClear: PropTypes.func,
    onEnter: PropTypes.func,
};

export default StyledTextInput;
