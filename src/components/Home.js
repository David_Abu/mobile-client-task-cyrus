import React, {useMemo, useState} from "react";
import MobileView from "./MobileView";
import styled from 'styled-components';
import Toggle from "./Toggler";
import StyledTextInput from "./StyledTextInput";
import StackOverflowService from "../services/StackOverflow";
import {debounce} from "lodash";
import User from "./User";
import {getUserProfileDetails} from "../utilities/utils";
import Questions from "./Questions";

const HomeWrap = styled.div`
  padding: 1em 1.5em;
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 100%;
  overflow: hidden;
`;

const stackOverflowService = new StackOverflowService();

const PageHeader = styled.h3`
  padding-bottom: 30px;

`;

const Home = ({theme, toggleTheme}) => {
    const [inputUserId, setInputUserId] = useState('');
    const [userProfile, setUserProfile] = useState(null);
    const [questions, setQuestions] = useState(null);

    function getUserQuestions(userId) {
        return stackOverflowService.getUserQuestions(userId)
    }

    function setUserQuestions(userId) {
        getUserQuestions(userId).then(res => {
            const userProfile = res?.items && res.items[0] && res.items[0].owner;
            setUserProfile(getUserProfileDetails(userProfile));
            setQuestions(res?.items)
        }).catch(err => {
            console.log('setUserQuestions: ', err)
        })
    }

    const debounceGetUserQs = useMemo(
        () => debounce((id) => setUserQuestions(id), 500)
        , []);// eslint-disable-line

    function onInputUserId({target: {value}}) {
        setInputUserId(value)
        debounceGetUserQs(value);
    }

    function onClear() {
        setInputUserId('');
        setUserProfile(null);
        setQuestions(null);
    }

    function onEnterClick() {
        setUserQuestions(inputUserId);
    }


    return (
        <MobileView>
            <HomeWrap>
                <Toggle
                    theme={theme}
                    toggleTheme={toggleTheme}
                />
                <PageHeader>
                    Get Stack overflow posts
                </PageHeader>
                <StyledTextInput
                    input={inputUserId}
                    onChange={onInputUserId}
                    placeholder={'enter user id'}
                    onClear={onClear}
                    onEnter={onEnterClick}
                    data-testid="input-user-id"
                />
                <div style={{height: '30px'}}/>

                {
                    !!userProfile &&
                    <User
                        {...userProfile}
                    />
                }

                {
                    !!questions &&
                    <Questions
                        questions={questions}
                    />
                }

            </HomeWrap>
        </MobileView>
    );
};

export default Home;