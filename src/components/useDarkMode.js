import {useEffect, useState} from 'react';
import {LOCAL_STORAGE} from "../utilities/const";

export const DARK_MODE = {
    light: 'light',
    dark: 'dark'
}

export const useDarkMode = () => {
    const [theme, setTheme] = useState(DARK_MODE.light);
    const setMode = mode => {
        window.localStorage.setItem(LOCAL_STORAGE.theme, mode)
        setTheme(mode);
    };

    const toggleTheme = () => {
        theme === DARK_MODE.light ? setMode(DARK_MODE.dark) : setMode(DARK_MODE.light)
    };

    useEffect(() => {
        const localTheme = window.localStorage.getItem(LOCAL_STORAGE.theme);
        if (localTheme) {
            setTheme(localTheme);
        } else {
            //check if the user is using dark mode theme
            if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
                setMode(DARK_MODE.dark);
            } else {
                setMode(DARK_MODE.light);
            }
        }
    }, []);

    return [theme, toggleTheme]
};