import React from "react";
import styled from "styled-components";
import {number, string} from "prop-types";


const UserWrap = styled.div`
  display: flex;
  align-items: center;
`;

const Avatar = styled.img`
  border-radius: 10px;
  width: 65px;
  height: 65px;
  margin: 0 15px;
  background: url('${({src}) => src}');
`;


const UserDetails = styled.div`

`;

const Text = styled.div`

`;

const User = ({avatar, name, reputation, acceptRate}) => {

    return (
        <UserWrap>
            <Avatar
                src={avatar}
            />
            <UserDetails>
                <Text>
                    Display Name: {name}
                </Text>

                <Text>
                    Reputation: {reputation}
                </Text>

                <Text>
                    Accept Rate: {acceptRate}
                </Text>
            </UserDetails>
        </UserWrap>
    );
};

User.propTypes = {
    avatar: string,
    name: string,
    reputation: number,
    acceptRate: number,
}

export default User;

