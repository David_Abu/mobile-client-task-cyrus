import React, {useState} from "react";
import {sortBy} from "lodash";

import styled from "styled-components";
import Button from "react-bootstrap/Button";
import {Accordion, ButtonGroup} from "react-bootstrap";
import {SORT_MODE} from "../utilities/const";
import PropTypes from "prop-types";
import StyledTextInput from "./StyledTextInput";

const QuestionsWrap = styled.div`
  display: flex;
  align-items: center;
  padding: 30px 0 0 0;
  flex-direction: column;
  height: 100%;
  overflow-y: auto;
  justify-content: space-between;
`;

const QuestionsFilterWrap = styled.div`
  height: 100%;
  overflow-y: auto;
`;


const SortWrap = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 30px;
`;

const Text = styled.div`
  padding-right: 10px;
`;

const AccordionOuter = styled.div`
  overflow-y: auto;

`;

const AccordionWrap = styled(Accordion)`
  background-color: inherit;
  width: 100%;
  max-width: 100%;

  .accordion-button {
    background-color: inherit;
    color: inherit;
  }

`;

const AccordionItem = styled(Accordion.Item)`
  background-color: inherit;
`;

const Preview = styled.embed`
  width: 100%;
  max-width: 100%;

`;

const QuestionCount = styled.div`
  padding: 10px;
`;

const Questions = ({questions}) => {
    const [sortMode, setSortMode] = useState('Date');

    function onClick(e) {
        const {target: {value}} = e;
        setSortMode(value);
    }

    function getSortedQs(questions) {
        let sortKey = 'creation_date';
        if (sortMode === SORT_MODE.answers) {
            sortKey = 'answer_count'
        }

        if (sortMode === SORT_MODE.views) {
            sortKey = 'answer_count'
        }

        return sortBy(questions, [sortKey])

    }

    const sortedQs = getSortedQs(questions);
    return (
        <QuestionsWrap>
            <QuestionsFilterWrap>
                <SortWrap>
                    <Text>Sort By: </Text>
                    <ButtonGroup aria-label="Basic example">
                        <Button
                            onClick={onClick}
                            value={SORT_MODE.date}
                            variant="secondary"
                        >
                            {SORT_MODE.date}
                        </Button>

                        <Button
                            onClick={onClick}
                            value={SORT_MODE.answers}
                            variant="secondary"
                        >
                            {SORT_MODE.answers}
                        </Button>
                        <Button
                            onClick={onClick}
                            value={SORT_MODE.views}
                            variant="secondary"
                        >
                            {SORT_MODE.views}
                        </Button>
                    </ButtonGroup>
                </SortWrap>
                <AccordionOuter>
                    <AccordionWrap>
                        {
                            sortedQs.map((question, index) => {
                                const {title, question_id, link} = question;
                                return (
                                    <AccordionItem
                                        key={question_id}
                                        eventKey={index}
                                    >
                                        <Accordion.Header>{title}</Accordion.Header>
                                        <Accordion.Body>
                                            <Preview
                                                src={link}
                                                width="100%" height="100%"
                                            />
                                        </Accordion.Body>
                                    </AccordionItem>
                                )
                            })
                        }
                    </AccordionWrap>
                </AccordionOuter>
            </QuestionsFilterWrap>
            <QuestionCount>
                Total of {questions.length} questions found
            </QuestionCount>
        </QuestionsWrap>
    );
};

StyledTextInput.propTypes = {
    questions: PropTypes.array,
};

export default React.memo(Questions);