import React from 'react';
import styled from 'styled-components';
import PropTypes from "prop-types";


const TextInputWrap = styled.input`
  background: none;
  outline: none;
  border: none;
  border-bottom: 2px solid #d9d9d9;
  padding: 5px 20px;
  width: 100%;
  color: inherit;

  &:focus {
    border-bottom: 2px solid #a2a2a2;
  }

  ::placeholder,
  ::-webkit-input-placeholder {
    font-size: 100%;
    color: #999999;
  }

  :-ms-input-placeholder {
    font-size: 100%;
    color: #999999;
  }
`;

function TextInput({
                       setRef = () => {
                       },
                       type = 'text',
                       value,
                       onEnter,
                       ...props
                   }) {

    return (
        <TextInputWrap
            ref={setRef}
            type={type}
            value={value}
            {...props}
            onKeyDown={(event) => {
                if (event.key === "Enter") {
                    onEnter && onEnter()
                }
            }}
        />
    );
}

TextInput.propTypes = {
    setRef: PropTypes.func,
    onEnter: PropTypes.func,
    type: PropTypes.string,
    value: PropTypes.string,
};


export default TextInput;
