import React from "react";

import styled from "styled-components";

const Mobile = styled.div`
  position: relative;
  margin: 40px auto;
  width: 360px;
  height: 780px;
  border-radius: 40px;
  box-shadow: 0 0 0 11px #1f1f1f, 0 0 0 13px #191919, 0 0 0 20px #111;

`;

const MobileView = ({children}) => {

    return (
        <Mobile>
            {children}
        </Mobile>
    );
};

export default MobileView;