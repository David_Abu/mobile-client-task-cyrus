import React from 'react';
import {render} from '@testing-library/react';
import App from './App';

test('expect app component', () => {
    const {getByTestId} = render(<App/>);
    const linkElement = getByTestId('App');
    expect(linkElement).toBeInTheDocument();
});

test('expect app component', () => {
    const {getByTestId} = render(<App/>);
    const linkElement = getByTestId('input-user-id');
    expect(linkElement).toBeInTheDocument();
});
